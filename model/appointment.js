const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull,
  GraphQLScalarType,
} = require("graphql");
const expressGraphQL = require("express-graphql").graphqlHTTP;
const axios = require("axios");

const AppointmentType = new GraphQLObjectType({
  name: "appointmentByClient",
  description: "This represents a Appointment ",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    communicationMethod: { type: GraphQLNonNull(GraphQLString) },
    dateBegin: { type: GraphQLNonNull(GraphQLFloat) },
    dateEnd: { type: GraphQLNonNull(GraphQLFloat) },
    dateAdded: { type: GraphQLNonNull(GraphQLFloat) },

    //authorId: { type: GraphQLNonNull(GraphQLInt) },
  }),
});

const RootQueryType = new GraphQLObjectType({
  name: "Query",
  description: "Root Query",
  fields: () => ({
    appointments: {
      type: new GraphQLList(AppointmentType),
      description: "List of all appointments ",
      resolve: () => global.appo,
    },
    appointmentByClient: {
      type: new GraphQLList(AppointmentType),
      args: {
        clientContactReferenceID: { type: GraphQLInt },
      },
      resolve(parent, args) {
        console.log("args", args);
        return axios
          .get(`http://localhost:5000/api/v1/client/${args.clientContactReferenceID}`)
          .then((response) => {
            console.log("response", response);
            return response.data  ;
            // for (let todo of response.data) {
            //   console.log(todo);
            // }
          })
          .catch(console.error);
      },
    },
  }),
});

module.exports = { AppointmentType, RootQueryType };

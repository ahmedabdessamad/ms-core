const express = require("express");
const axios = require("axios");
const cors = require("cors");
require("dotenv").config();
const {
  GraphQLSchema,
} = require("graphql");
const expressGraphQL = require("express-graphql").graphqlHTTP;
const clientref = 118565;
const Appointment = require("./model/appointment");

axios
  .get("http://localhost:5000/api/v1/client/" + clientref)
  .then((response) => {
    global.appo = response.data;
    // for (let todo of response.data) {
    //   console.log(todo);
    // }
  })
  .catch(console.error);

const schema = new GraphQLSchema({
  query: Appointment.RootQueryType,
});

const app = express();
const corsOptions = {
  origin: "http://localhost:5500/",
  Credential: true,
};

app.use(cors());

app.use(
  "/graphql",
  expressGraphQL({
    schema: schema,
    graphiql: true,
  })
);

app.listen(process.env.PORT, () =>
  console.log("Server Started at Port " + process.env.PORT)
);
